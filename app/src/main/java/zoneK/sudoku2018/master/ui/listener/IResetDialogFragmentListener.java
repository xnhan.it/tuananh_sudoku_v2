package zoneK.sudoku2018.master.ui.listener;

/**
 * Created by Chris on 19.01.2016.
 */
public interface IResetDialogFragmentListener {
    public void onResetDialogPositiveClick();
    public void onDialogNegativeClick();
}
