package zoneK.sudoku2018.master.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.ads.MobileAds;
import zoneK.sudoku2018.master.ui.view.R;

import zoneK.sudoku2018.master.ui.view.AdmobPopupAd;

/**
 * Created by yonjuni on 22.10.16.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MobileAds.initialize(this,getString(R.string.app_id));
        new AdmobPopupAd(this);
        AdmobPopupAd.loadInterstitial();
        Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
        SplashActivity.this.startActivity(mainIntent);
        SplashActivity.this.finish();
    }

}