package zoneK.sudoku2018.master.ui.view;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import zoneK.sudoku2018.master.controller.GameController;
import zoneK.sudoku2018.master.controller.Symbol;
import zoneK.sudoku2018.master.game.listener.IHighlightChangedListener;
import zoneK.sudoku2018.master.ui.view.R;

/**
 * Created by TMZ_LToP on 12.11.2015.
 */


public class SudokuKeyboardLayout extends LinearLayout implements IHighlightChangedListener {

    AttributeSet attrs;
    SudokuButton[] buttons;
    GameController gameController;
    Symbol symbolsToUse = Symbol.Default;
    float normalTextSize = 20; // in sp
    LinearLayout [] layouts = new LinearLayout[2];
    LinearLayout middleLL;
    FragmentManager fm;

    OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v instanceof SudokuButton) {
                SudokuButton btn = (SudokuButton)v;

                gameController.selectValue(btn.getValue());
            }

            switch (v.getId()) {
                case R.id.clear_btn:
                    gameController.deleteSelectedCellsValue();
                    break;
                case R.id.onoff_btn:
                    // rotates the Drawable
                    gameController.setNoteStatus(!gameController.getNoteStatus());
                    updateNotesEnabled();
                    onHighlightChanged();
                    break;
                case R.id.undo_btn:
                    gameController.UnDo();
                    break;
//                case Undo:
//                    gameController.UnDo();
//                    break;
                case R.id.hint_btn:
                    if(gameController.isValidCellSelected()) {
                        if(AdmobPopupAd.isInterstitialAdLoaded())
                        {
                            AdmobPopupAd.showInterstitial();
                            AdmobPopupAd.setInterstitialAdListener(new AdmobPopupAd.InterstitialAdListener() {
                                @Override
                                public void onLoaded() {

                                }

                                @Override
                                public void onFailed() {

                                }

                                @Override
                                public void onClosed() {
                                    AdmobPopupAd.setInterstitialAdListener(null);
                                    if(gameController.getUsedHints() == 0) {
                                        // are you sure you want to use a hint?
                                        SudokuSpecialButtonLayout.HintConfirmationDialog hintDialog = new SudokuSpecialButtonLayout.HintConfirmationDialog();
                                        hintDialog.show(fm, "HintDialogFragment");

                                    } else {
                                        gameController.hint();
                                    }
                                }
                            });
                        }else
                        {
                            if(gameController.getUsedHints() == 0) {
                                // are you sure you want to use a hint?
                                SudokuSpecialButtonLayout.HintConfirmationDialog hintDialog = new SudokuSpecialButtonLayout.HintConfirmationDialog();
                                hintDialog.show(fm, "HintDialogFragment");

                            } else {
                                gameController.hint();
                            }
                        }
                    } else {
                        // Display a Toast that explains how to use the Hint function.
                        Toast t = Toast.makeText(getContext(), R.string.hint_usage, Toast.LENGTH_SHORT);
                        t.show();
                    }
                    break;
                default:
                    break;
            }
        }
    };


    public SudokuKeyboardLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.attrs = attrs;
    }

    public void setSymbols(Symbol s) {
        symbolsToUse = s;
        for(SudokuButton b : buttons) {
            b.setText(Symbol.getSymbol(symbolsToUse, b.getValue()-1));
        }
    }

    public void setKeyBoard(int size,int width, int height, int orientation) {
        LayoutParams p;
        int number = 0;
//        int numberOfButtonsPerRow = (size % 2 == 0) ? size/2 :(size+1)/2;
//        int numberOfButtons = numberOfButtonsPerRow * 2;
        int numberOfButtonsPerRow = (size % 2 == 0) ? size :(size);;
        int numberOfButtons = numberOfButtonsPerRow * 1;

        normalTextSize = (int) getResources().getDimension(R.dimen.number_size) / getResources().getDisplayMetrics().scaledDensity;

        buttons = new SudokuButton[numberOfButtons];

        // Middle layout
        View layout2 = LayoutInflater.from(getContext()).inflate(R.layout.middle, middleLL, false);
        addView(layout2);

        ((ImageView) findViewById(R.id.undo_btn)).setOnClickListener(listener);
        ((ImageView) findViewById(R.id.clear_btn)).setOnClickListener(listener);
        ((ImageView) findViewById(R.id.hint_btn)).setOnClickListener(listener);
        ((ImageView) findViewById(R.id.onoff_btn)).setOnClickListener(listener);

        //set layout parameters and init Layouts
        for (int i = 0; i < 2; i++) {
            if(orientation == LinearLayout.HORIZONTAL) {
                p = new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1);
            } else {
                p = new LayoutParams(0, LayoutParams.MATCH_PARENT, 1);
            }
            //if (i == 0) p.bottomMargin=10; else p.topMargin=10;
            p.setMargins(0, 0, 0, 0);
            layouts[i] = new LinearLayout(getContext(),null);
            layouts[i].setLayoutParams(p);
            layouts[i].setWeightSum(numberOfButtonsPerRow);
            layouts[i].setOrientation(orientation);
            addView(layouts[i]);
        }

        for (int layoutNumber = 0; layoutNumber < 1 ; layoutNumber++){
            for (int i = 0; i < numberOfButtonsPerRow; i++){
                int buttonIndex = i + layoutNumber * numberOfButtonsPerRow;
                buttons[buttonIndex] = new SudokuButton(getContext(),null);
                if(orientation == LinearLayout.HORIZONTAL) {
                    p = new LayoutParams(0, LayoutParams.MATCH_PARENT, 1);
                } else {
                    p = new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1);
                }
                p.setMargins(1,1,1,1);
                buttons[buttonIndex].setLayoutParams(p);
                /* removed GridLayout because of bad scaling will use now a Linearlayout
                Spec rowSpec = spec(k,1);
                Spec colSpec = spec(i,1);

                p = (new LayoutParams(rowSpec,colSpec));

                //p = new LayoutParams(rowSpec,colSpec);
                p.setMargins((i == 0) ? 0 : 5,5,5,5);
                p.width = (width - (int)((getResources().getDimension(R.dimen.activity_horizontal_margin))*2)) / realSize;
                p.width = p.width - 10;
                //p.setGravity(Gravity.FILL_VERTICAL);
                //p.setGravity(Gravity.FILL);
               // p.setGravity(LayoutParams.WRAP_CONTENT);
                */

          //      buttons[number].setLayoutParams(p);
                buttons[buttonIndex].setType(SudokuButtonType.Value);
                buttons[buttonIndex].setTextColor(getResources().getColor(R.color.dark_green_color));
//                buttons[buttonIndex].setBackgroundResource(R.drawable.mnenomic_numpad_button);
                buttons[buttonIndex].setBackgroundResource(R.drawable.mnenomic_numpad_button);
                buttons[buttonIndex].setPadding(0, 0, 0, 0);
                buttons[buttonIndex].setGravity(Gravity.CENTER);
                buttons[buttonIndex].setText(Symbol.getSymbol(symbolsToUse, buttonIndex));
                buttons[buttonIndex].setTextSize(TypedValue.COMPLEX_UNIT_SP, normalTextSize);
                buttons[buttonIndex].setValue(buttonIndex + 1);
                buttons[buttonIndex].setOnClickListener(listener);

                if (buttonIndex == size) {
                    buttons[buttonIndex].setVisibility(INVISIBLE);
                }

                layouts[layoutNumber].addView(buttons[buttonIndex]);
            }
        }
    }

    public void setButtonsEnabled(boolean enabled) {
        for(SudokuButton b : buttons) {
            b.setEnabled(enabled);
        }
    }

    public void setGameController(GameController gc){
        if(gc == null) {
            throw new IllegalArgumentException("GameController may not be null.");
        }

        gameController = gc;
        gameController.registerHighlightChangedListener(this);
    }

    public void updateNotesEnabled() {

        if(gameController.getNoteStatus()) {
            setTextSize(TypedValue.COMPLEX_UNIT_SP,normalTextSize*0.55f);
        } else {
            setTextSize(TypedValue.COMPLEX_UNIT_SP,normalTextSize);
        }
    }

    private void setTextSize(int unit,float size){
        for (SudokuButton b : buttons){
            //b.setTextSize(size);
            b.setTextSize(unit,size);
        }
    }

    public void setFragment(FragmentManager fm) {
        this.fm = fm;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public void onHighlightChanged() {
        for(SudokuButton i_btn : buttons) {
            i_btn.setBackgroundResource(R.drawable.mnenomic_numpad_button);

            // Highlight Yellow if we are done with that number
            if(gameController.getValueCount(i_btn.getValue()) == gameController.getSize()) {
                i_btn.setBackgroundResource(R.drawable.numpad_highlighted_three);
                i_btn.setTextColor(getResources().getColor(R.color.dark_green_color));
            }

            if(gameController.getSelectedValue() == i_btn.getValue()) {
                // highlight button to indicate that the value is selected
                i_btn.setBackgroundResource(R.drawable.numpad_highlighted);
                i_btn.setTextColor(getResources().getColor(R.color.dark_green_color));
            }
        }
    }


}
