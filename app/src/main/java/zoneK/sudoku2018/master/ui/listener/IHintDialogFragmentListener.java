package zoneK.sudoku2018.master.ui.listener;

/**
 * Created by Chris on 17.01.2016.
 */
public interface IHintDialogFragmentListener {
    public void onHintDialogPositiveClick();
    public void onDialogNegativeClick();
}
