package zoneK.sudoku2018.master.game.listener;

/**
 * Created by Chris on 21.01.2016.
 */
public interface IHintListener {
    public void onHintUsed();
}
