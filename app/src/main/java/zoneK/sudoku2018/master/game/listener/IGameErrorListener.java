package zoneK.sudoku2018.master.game.listener;

import java.util.List;

import zoneK.sudoku2018.master.game.CellConflict;

/**
 * Created by Chris on 02.02.2016.
 */
public interface IGameErrorListener {
    public void onGameFilledWithErrors(List<CellConflict> errorList);
}
