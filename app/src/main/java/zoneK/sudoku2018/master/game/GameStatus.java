package zoneK.sudoku2018.master.game;

/**
 * Created by Chris on 10.11.2015.
 */
public enum GameStatus {
    Unspecified,
    Running,
    Paused,

}
