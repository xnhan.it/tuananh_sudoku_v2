package zoneK.sudoku2018.master.game.listener;

/**
 * Created by Chris on 19.11.2015.
 */
public interface IGameSolvedListener {
    public void onSolved();
}
