package zoneK.sudoku2018.master.game.listener;

import zoneK.sudoku2018.master.game.GameCell;

/**
 * Created by Chris on 19.11.2015.
 */
public interface IModelChangedListener {
    public void onModelChange(GameCell c);
}
