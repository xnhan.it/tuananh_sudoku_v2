package zoneK.sudoku2018.master.game.listener;

/**
 * Created by Chris on 19.12.2015.
 */
public interface IHighlightChangedListener {
    public void onHighlightChanged();
}
